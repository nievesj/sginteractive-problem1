﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sginteractive_problem1
{
    class YearSummary
    {
        public int Year { get; set; }
        public int PeopleAlive { get; set; }

        public YearSummary() { }

        public YearSummary(int year, int peoplealive)
        {
            Year = year;
            PeopleAlive = peoplealive;
        }
    }
}
