﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sginteractive_problem1
{
    public class Person
    {
        public string Name { get; set; }
        public int BirthYear { get; set; }
        public int DeathYear { get; set; }

        public Person() {}

        public Person(string name, int bitrh, int death)
        {
            this.Name = name;
            this.BirthYear = bitrh;
            this.DeathYear = death;
        }  
    }
}
