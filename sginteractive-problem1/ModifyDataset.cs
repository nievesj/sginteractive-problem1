﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sginteractive_problem1
{
  
    public partial class ModifyDataset : Form
    {
        private MainForm parent;

        public ModifyDataset(string json)
        {
            InitializeComponent();

            txtData.Text = json;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //update count and chart
            parent.UpdateDataset(txtData.Text);

            this.Close();
        }

        private void btnRevert_Click(object sender, EventArgs e)
        {
            string json = parent.ReadData();
            txtData.Text = json;
        }

        private void ModifyDataset_Load(object sender, EventArgs e)
        {
            //get parent reference
            parent = (MainForm)this.Owner;
        }
    }
}
