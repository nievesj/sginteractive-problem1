﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.IO;
using Newtonsoft.Json;
using System.Reflection;

namespace sginteractive_problem1
{
    /// <summary>
    /// Author: José M. Nieves
    /// Purpose: SGInteractive programming test
    /// Description: Given a list of people with their birth and end years (all between 1900 and 2000), find the year with the most number of people alive.
    /// </summary>
    public partial class MainForm : Form
    {
        private string _json;

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //read json file
            _json = ReadData();

            //create report
            PresentData();
        }

        private void PresentData()
        {
            //convert json file to objects
            List<Person> persons = JsonToObjects(_json);

            //process
            CountPeople(persons);
        }

        private void CountPeople(List<Person> persons)
        {
            List <YearSummary> yearSummary = new List<YearSummary>();

            int YearWithMostPeopleAlive = 0;
            int PeopleAlive = 0;    

            //loop from 1900 to 2000
            for (int i = 1900; i <= 2000; i++)
            {
                int AliveCurrentYear = 0;

                //loop persons and check if the person is alive for the current year
                foreach (Person person in persons)
                {
                    if (i >= person.BirthYear && i <= person.DeathYear)
                    {
                        //yay! it's alive!! Count in for the total of current year
                        AliveCurrentYear = AliveCurrentYear + 1;
                    }
                }

                //for chart, if there is a person alive in current year, add it to list
                if(AliveCurrentYear > 0)
                {
                    yearSummary.Add(new YearSummary(i, AliveCurrentYear));
                }

                //check if the total people alive for this current year is greater than the total alive ever
                if (AliveCurrentYear > PeopleAlive)
                {
                    //more people alive in this year than in previous years! update information!
                    PeopleAlive = AliveCurrentYear;
                    YearWithMostPeopleAlive = i;
                }
            }

            txtSummary.Text = "Year with most people alive is " + YearWithMostPeopleAlive + " with a total of " + PeopleAlive + " persons.";

            //bind and setup chart
            DataChart.DataSource = new BindingList<YearSummary>(yearSummary);
            DataChart.DataBind();
            DataChart.Update();

            DataChart.Series.First().XValueMember = "Year";
            DataChart.Series.First().YValueMembers = "PeopleAlive";
        }

        /// <summary>
        /// Reads the .json file and returns the string
        /// </summary>
        /// <returns></returns>
        public string ReadData()
        {
            //Get .exe path and combine it with the data.json file name
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Data\data.json");

            //read all file in one go
            string json = File.ReadAllText(path);

            return json;
        }

        /// <summary>
        /// update dataset
        /// </summary>
        /// <param name="json"></param>
        public void UpdateDataset(string json)
        {
            _json = json;
            PresentData();
        }

        /// <summary>
        /// convert json string to objects
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        private List<Person> JsonToObjects(string json)
        {
            List<Person> persons = new List<Person>();

            try
            {
                persons = JsonConvert.DeserializeObject<List<Person>>(json);
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                MessageBox.Show("JSON string is not valid, please check your modifications or revert data.");
            }

            return persons;
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            ModifyDataset modForm = new ModifyDataset(_json);

            modForm.ShowDialog(this);
        }
    }
}
