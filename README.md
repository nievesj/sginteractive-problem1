# Description #

Project reads a JSON file that contains an array of Persons with their name, birth date and death date. The program counts these persons to determine which is the year with the most persons alive, and how many people were alive. 

* SGInteractive Programming test - Problem 1
* Project will be available on my public BitBucket profile for a short while (from 2 to 3 weeks) after being submitted.

### Tools Used ###
* [Visual Studio 2015 Community Edition](https://www.visualstudio.com/en-us/products/visual-studio-community-vs.aspx)
* [JSON.Net](http://www.newtonsoft.com/json)
* WindowsForms
* SourceTree